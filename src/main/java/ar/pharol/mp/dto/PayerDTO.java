package ar.pharol.mp.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Información del Comprador.
 * 
 */
@Getter @Setter
public class PayerDTO {
	
	public String email;

}
