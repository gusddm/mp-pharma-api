package ar.pharol.mp.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Getter;
import lombok.Setter;

/**
 * Pago del Comprador por el total de la transacción.
 */
@Getter @Setter
public class DisbursementDTO {

	public Integer amount;

	@JsonAlias("external_reference")
	public String externalReference;
	
	@JsonAlias("collector_id")
	public Integer collectorId;
	
	@JsonAlias("application_fee")
	public Integer applicationFee;
	
	@JsonAlias("money_release_days")
	public Integer moneyReleaseDays;
}
