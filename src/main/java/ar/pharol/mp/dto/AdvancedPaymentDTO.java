package ar.pharol.mp.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AdvancedPaymentDTO {
	public PayerDTO payer;
	public List<PaymentDTO> payments;
	public List<DisbursementDTO> disbursements;
	@JsonAlias("external_reference")
	public String externalReference;		
}
