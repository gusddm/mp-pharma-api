package ar.pharol.mp.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Getter;
import lombok.Setter;

/**
 * Pago del Comprador por el total de la transacción.
 */
@Getter @Setter
public class PaymentDTO {

	@JsonAlias("payment_method_id")
	public String paymentMethodId;

	@JsonAlias("payment_type_id")
	public String paymentTypeId;

	public String token;
	
	@JsonAlias("transaction_amount")
	public Integer transactionAmount;
	
	public Integer installments;
	
	@JsonAlias("processing_mode")
	public String processingMode;
}