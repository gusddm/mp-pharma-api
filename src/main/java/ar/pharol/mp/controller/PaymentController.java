package ar.pharol.mp.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mercadopago.exceptions.MPException;
import com.mercadopago.resources.AdvancedPayment;

import ar.pharol.mp.dto.AdvancedPaymentDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@RestController
public class PaymentController {
	
	MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

	@PostMapping("/payment/split")
	@ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
	public String helloWorld(@RequestBody AdvancedPaymentDTO body, @RequestParam(value="access_token") String access_token) {
		
		
		MapperFacade mapper = mapperFactory.getMapperFacade();
		
		AdvancedPayment advancedPayment = mapper.map(body, AdvancedPayment.class);
		advancedPayment.setMarketplaceAccessToken(access_token);
		
		try {
			advancedPayment.save();
			return advancedPayment.getLastApiResponse().getStringResponse();
		} catch (MPException e) {
			return e.getMessage();
		}
	}
}